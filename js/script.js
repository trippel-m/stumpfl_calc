/*jslint vars: true, plusplus: true, devel: true, nomen: true, indent: 4, maxerr: 50*/
/*jslint browser: true*/
/*global $*/
function stumpfl_calc() {
    "use strict";

    var border = parseInt($('#border').val(), 10),
        horisontal_length = parseInt($('#horisontal').val(), 10) + (border * 2),
        horisontal_pieces_whole = Math.floor(((horisontal_length - 210) / 110)),
        horisontal_pieces_special = (horisontal_length - 210) - (horisontal_pieces_whole * 110),
        horisontal_pieces_special_amount,
        vertical_length = parseInt($('#vertical').val(), 10) + (border * 2),
        vertical_pieces_whole = Math.floor(((vertical_length - 210) / 110)),
        vertical_pieces_special = (vertical_length - 210) - (vertical_pieces_whole * 110),
        vertical_pieces_special_amount,
        horisontal_state,
        vertical_state,
        standard_total,
        erorMessageHeight = "Must be either 210 or greater than 230 high.",
        erorMessageWidth = "Must be either 210 or greater than 230 wide.";
    
    
    // Sørger for at den ikke tegner lerret om en variabel ikke er satt
    if (isNaN(vertical_length)) {
        vertical_length = 0;
        return;
    } else if (isNaN(horisontal_length)) {
        horisontal_length = 0;
        return;
    }
    
    /* State 1 = ugyldig, state 2 = ingen spesialdeler, state 3 = todelt spesial, state 4 = normal + spesial */

    // Sjekker om det hele er over minste størrelse
    if (horisontal_length < 210) {
        $('#display_horisontal').text(erorMessageHeight);
        $('#display_horisontal_special').text("Invalid");
        horisontal_state = 1;
        $("#horisontalInput").addClass("has-error").removeClass("has-success");
        // Sjekker om størrelsen blir mindre enn minste spesialstørrelse
    } else if (horisontal_length > 210 && horisontal_length < 230) {
        $('#display_horisontal').text(erorMessageHeight);
        $('#display_horisontal_special').text("Invalid");
        horisontal_state = 1;
        $("#horisontalInput").addClass("has-error").removeClass("has-success");
    // Sjekker om størrelsen går opp uten spesialdeler
    } else if (((horisontal_length - 210) / 110) % 1 === 0) {
        $('#display_horisontal').text(((horisontal_length - 210) / 110) * 2);
        $('#display_horisontal_special').text("None");
        horisontal_state = 2;
        $("#horisontalInput").addClass("has-success").removeClass("has-error");
    // Regner ut spesialstørrelse hvis spesialdel er for liten. Spesialdel kan ikke være mindre enn 20 cm
    } else if (horisontal_pieces_special < 20) {
        $('#display_horisontal').text((horisontal_pieces_whole - 1) * 2);
        $('#display_horisontal_special').text(((horisontal_pieces_special + 110) / 2).toFixed(2) + " cm * 2 placed next to the bottom corners");
        horisontal_state = 3;
        $("#horisontalInput").addClass("has-success").removeClass("has-error");
    // Lister ut størrelse og spesialdel hvis man er innenfor standardområdet
    } else {
        $('#display_horisontal').text(horisontal_pieces_whole * 2);
        $('#display_horisontal_special').text(horisontal_pieces_special.toFixed(2) + " cm pieces next to the right corners");
        horisontal_state = 4;
        $("#horisontalInput").addClass("has-success").removeClass("has-error");
    }


    // Sjekker om det hele er over minste størrelse
    if (vertical_length < 210) {
        $('#display_vertical').text(erorMessageWidth);
        $('#display_vertical_special').text("Invalid");
        vertical_state = 1;
        $("#verticalInput").addClass("has-error").removeClass("has-success");
    // Sjekker om størrelsen blir mindre enn minste spesialstørrelse
    } else if (vertical_length > 210 && vertical_length < 230) {
        $('#display_vertical').text(erorMessageWidth);
        $('#display_vertical_special').text("Invalid");
        vertical_state = 1;
        $("#verticalInput").addClass("has-error").removeClass("has-success");
    // Sjekker om størrelsen går opp uten spesialdeler
    } else if (((vertical_length - 210) / 110) % 1 === 0) {
        $('#display_vertical').text(((vertical_length - 210) / 110) * 2);
        $('#display_vertical_special').text("None");
        vertical_state = 2;
        $("#verticalInput").addClass("has-success").removeClass("has-error");
    // Regner ut spesialstørrelse hvis spesialdel er for liten. Spesialdel kan ikke være mindre enn 20cm
    } else if (vertical_pieces_special < 20) {
        $('#display_vertical').text((vertical_pieces_whole - 1) * 2);
        $('#display_vertical_special').text((((vertical_pieces_special + 110) / 2)).toFixed(2) + " cm * 2 placed next to the top and bottom corners");
        vertical_state = 3;
        $("#verticalInput").addClass("has-success").removeClass("has-error");
    // Lister ut størrelse og spesialdel hvis man er innenfor standardområdet
    } else {
        $('#display_vertical').text((vertical_pieces_whole * 2) + " pieces");
        $('#display_vertical_special').text((vertical_pieces_special.toFixed(2)) + " cm pieces next to the bottom corners");
        vertical_state = 4;
        $("#verticalInput").addClass("has-success").removeClass("has-error");
    }

    function colordot(size) {
        return "<span class='colordot color_" + size + "'></span>";
    }

    //Funksjon som tegner lerret
    function draw_screen() {
        var standardHTML = '<div id="drawing"><div id="topRow"></div><div id="leftColumn"></div><div id="rightColumn"></div><div id="bottomRow"></div></div>',
            standardLength = 110,
            special_horisontalLength = horisontal_pieces_special,
            special_verticalLength = vertical_pieces_special,
            horisontal_offset = 200 + (horisontal_pieces_whole * 110) + horisontal_pieces_special,
            vertical_offset = 95 + (vertical_pieces_whole * 110) + vertical_pieces_special;

        function draw_base() {
            $("#drawing").replaceWith(standardHTML);
        }


        function draw_horisontal() {
            var i;
            $('#topRow').append('<div class="topleft element"></div>');

            if (horisontal_state === 3) {
                special_horisontalLength = (special_horisontalLength + 110) / 2;
                $('#topRow').append('<div class="element color_' + special_horisontalLength + '" style="width: ' + special_horisontalLength + 'px;"><div class="part"><p>' + special_horisontalLength.toFixed(2) + '</p></div></div>');
                horisontal_pieces_whole = horisontal_pieces_whole - 1;
            }

            for (i = 0; i < horisontal_pieces_whole; i++) {
                $('#topRow').append('<div class="element color_' + standardLength + '" style="width: ' + standardLength + 'px;"><div class="part"><p>' + standardLength.toFixed(2) + '</p></div></div>');
            }

            if (horisontal_state === 3 || horisontal_state === 4) {
                $('#topRow').append('<div class="element color_' + special_horisontalLength + '" style="width: ' + special_horisontalLength + 'px;"><div class="part"><p>' + special_horisontalLength.toFixed(2) + '</p></div></div>');
            }

            $('#topRow').append('<div class="topright element"></div>');

            $('#bottomRow').append('<div class="bottomleft element"></div>');

            if (horisontal_state === 3) {
                $('#bottomRow').append('<div class="element color_' + special_horisontalLength + '" style="width: ' + special_horisontalLength + 'px;"><div class="part"><p>' + special_horisontalLength.toFixed(2) + '</p></div></div>');
            }

            for (i = 0; i < horisontal_pieces_whole; i++) {
                $('#bottomRow').append('<div class="element color_' + standardLength + '" style="width: ' + standardLength + 'px;"><div class="part"><p>' + standardLength.toFixed(2) + '</p></div></div>');
            }

            if (horisontal_state === 3 || horisontal_state === 4) {
                $('#bottomRow').append('<div class="element color_' + special_horisontalLength + '" style="width: ' + special_horisontalLength + 'px;"><div class="part"><p>' + special_horisontalLength.toFixed(2) + '</p></div></div>');
            }

            $('#bottomRow').append('<div class="bottomright element"></div>').css({"top": vertical_offset});
            $('#drawing').css({"width": horisontal_offset + 10});
            $("#drawing").css("height", (vertical_offset + 105) + "px");


        }
        function draw_vertical() {
            var i;
            if (vertical_state === 3) {
                special_verticalLength = (special_verticalLength + 110) / 2;
                $('#leftColumn').append('<div class="element color_' + special_verticalLength + '" style="height: ' + special_verticalLength + 'px;"><div class="part"><p>' + special_verticalLength + '</p></div></div>');
                vertical_pieces_whole = vertical_pieces_whole - 1;
            }

            for (i = 0; i < vertical_pieces_whole; i++) {
                $('#leftColumn').append('<div class="element color_' + standardLength + '" style="height: ' + standardLength + 'px;"><div class="part"><p>' + standardLength + '</p></div></div>');
            }

            if (vertical_state === 3 || vertical_state === 4) {
                $('#leftColumn').append('<div class="element color_' + special_verticalLength + '" style="height: ' + special_verticalLength + 'px;"><div class="part"><p>' + special_verticalLength + '</p></div></div>');
            }

            if (vertical_state === 3) {
                $('#rightColumn').append('<div class="element color_' + special_verticalLength + '" style="height: ' + special_verticalLength + 'px;"><div class="part"><p>' + special_verticalLength + '</p></div></div>');
            }

            for (i = 0; i < vertical_pieces_whole; i++) {
                $('#rightColumn').append('<div class="element color_' + standardLength + '" style="height: ' + standardLength + 'px;"><div class="part"><p>' + standardLength + '</p></div></div>');
            }

            if (vertical_state === 3 || vertical_state === 4) {
                $('#rightColumn').append('<div class="element color_' + special_verticalLength + '" style="height: ' + special_verticalLength + 'px;"><div class="part"><p>' + special_verticalLength + '</p></div></div>');
            }
            $('#rightColumn').css({"left": horisontal_offset});
            
 

        }

        function draw_color() {
            var colorlist = {
                "0": "#000000",
                "1": "#632423",
                "2": "#c00000",
                "3": "#e36c0a",
                "4": "#ffff00",
                "5": "#00b050",
                "6": "#548dd4",
                "7": "#cb67fd",
                "8": "#949195",
                "9": "#ffffff"
            },
                x;

            $(".element, .colordot").each(function (index) {
                var classes = $(this).attr("class").split(" ");
                for (x = 0; x <= classes.length; x++) {
                    var c = classes[x];
                    if (c !== undefined) {
                        var cs = c.split("_");
                        if (cs[0] === "color") {
                            var color = cs[1];
                            color = color.replace(".", "");
                            color = color.replace("0", "");
                            if (color.substr(-1) === "0") {
                                color = color.substr(0, color.length - 1);
                            }
                            if (color.substr(0, 1) === color.substr(1, 1)) {
                                color = color.substr(0, 1);
                            }
                            if (color.length === 1) {
                                $(this).css('background-color', colorlist[color]);
                            } else if (color.length === 2) {
                                $(this).css('background', 'linear-gradient(45deg, ' + colorlist[color.substr(0, 1)] + ' 50%, ' + colorlist[color.substr(1, 1)] + '  50%)');
                            } else if (color.length === 3) {
                                $(this).css('background', 'linear-gradient(45deg, ' + colorlist[color.substr(0, 1)] + ' 33%, ' + colorlist[color.substr(1, 1)] + '  33%,' + colorlist[color.substr(2, 1)] + '  34%)');
                            } else if (color.length === 4) {
                                $(this).css('background', 'linear-gradient(45deg, ' + colorlist[color.substr(0, 1)] + ' 25%, ' + colorlist[color.substr(1, 1)] + '  25%,' + colorlist[color.substr(2, 1)] + '  25%,' + colorlist[color.substr(3, 1)] + '  25%)');
                            }

                        }
                    }
                }
            });
        }

        function list_parts() {
            standard_total = (vertical_pieces_whole + horisontal_pieces_whole) * 2;
            
            /* Sjekker om lerretsspesialdel må være todelt */
            if (vertical_state === 3) {
                vertical_pieces_special_amount = 4;
            } else {
                vertical_pieces_special_amount = 2;
            }
            
            /* Sjekker om lerretsspesialdel må være todelt */
            if (horisontal_state === 3) {
                horisontal_pieces_special_amount = 4;
            } else {
                horisontal_pieces_special_amount = 2;
            }

            if (special_horisontalLength === special_verticalLength) {
                if (standard_total > 0) {
                    $('#part1').html(colordot(standardLength) + standardLength.toFixed(2) + " cm");
                    $('#amount1').text(standard_total);
                    $('#part1').closest('tr').addClass("visible").removeClass("hidden");
                } else {
                    $('#part1').closest('tr').addClass("hidden").removeClass("visible");
                }
                if (vertical_pieces_special_amount > 0) {
                    $('#part2').html(colordot(special_verticalLength) + special_verticalLength.toFixed(2) + " cm");
                    $('#amount2').text(vertical_pieces_special_amount + horisontal_pieces_special_amount);
                } else {
                    $('#part2').closest('tr').remove();
                }

                $('#part3').html(colordot("9") + "Corner");
                $('#amount3').html(4);
                $('#part4').closest('tr').remove();
            } else {
                if (standard_total > 0) {
                    $('#part1').html(colordot(standardLength.toFixed(2)) + standardLength.toFixed(2) + " cm");
                    $('#amount1').text(standard_total);
                    $('#part1').closest('tr').addClass("visible").removeClass("hidden");
                } else {
                    $('#part1').closest('tr').addClass("hidden").removeClass("visible");
                    
                }
                if (vertical_pieces_special_amount > 0) {
                    $('#part2').html(colordot(special_verticalLength) + special_verticalLength.toFixed(2) + " cm");
                    $('#amount2').text(vertical_pieces_special_amount);
                } else {
                    $('#part2').closest('tr').remove();
                }
                if (special_horisontalLength > 0) {
                    $('#part3').html(colordot(special_horisontalLength) + special_horisontalLength.toFixed(2) + " cm");
                    $('#amount3').text(horisontal_pieces_special_amount);
                } else {
                    $('#part3').closest('tr').remove();
                }
                $('#part4').html(colordot("9") + "Corner");
                $('#amount4').html(4);
            }
            $('#partsList').addClass("visible").removeClass("hidden");
        }

        function clear_parts() {
            $('#part1').text('');
            $('#amount1').text('');
            $('#part2').text('');
            $('#amount2').text('');
            $('#part3').text('');
            $('#amount3').text('');
            $('#part4').text('');
            $('#amount4').text('');
            $('#partsList').addClass("hidden").removeClass("visible");
        }

        if (horisontal_state === 1 || vertical_state === 1) {
            draw_base();
        } else {
            clear_parts();
            draw_base();
            draw_horisontal();
            draw_vertical();
            list_parts();

        }

        draw_color();

    }

    draw_screen();
}

$('#horisontal').keyup(stumpfl_calc);
$('#vertical').keyup(stumpfl_calc);
